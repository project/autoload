<?php

/**
 * @file
 * PHP 5.3+ autoloader for objects.
 */

require_once 'autoload.cache.inc';

autoload();

/**
 * Returns the autoloading class map of a settled autoloader.
 *
 * @param bool $rebuild
 *   Tells whether the autoloading class map has to be rebuilt.
 *
 * @return \AutoloadCache
 *   The autoloading class map.
 */
function autoload($rebuild = FALSE) {
  static $callback;

  $map =& drupal_static(__FUNCTION__);

  if (NULL === $map) {
    $map = new \AutoloadCache(variable_get('autoload_file', sprintf('%s/%s.php', DRUPAL_ROOT, __FUNCTION__)));
    // Assume the autoloading mapping needs to re-registered if someone
    // requires this.
    $callback = NULL;
  }

  if ($rebuild) {
    if (NULL !== $callback && spl_autoload_unregister($callback)) {
      // Our autoloader has been deregistered and its updated state needs
      // to be registered repeatedly.
      $callback = NULL;
    }

    $map->rebuild();
  }

  if (NULL === $callback) {
    // Do not set an autoloading callback in a case when something went wrong.
    // An exception might be thrown during module installation when no database
    // schema available yet.
    try {
      $modules = module_list();
      $callback = function ($namespace) use ($map, $modules) {
        if (isset($map[$namespace])) {
          // Report about the problem but load class in any way.
          if (!isset($modules[$map[$namespace]['provider']]) && 'cli' !== PHP_SAPI) {
            trigger_error(sprintf('You are autoloading the "%s", provided by "%s" that is currently disabled!', $namespace, $map[$namespace]['provider']), E_USER_WARNING);
          }

          // The "DRUPAL_ROOT" constant is not prepended to allow including
          // files relative to the Drupal root as well as those, that are
          // located outside.
          require_once $map[$namespace]['file'];
        }
      };

      // Make this autoloader first to omit execution of Drupal ones.
      spl_autoload_register($callback, TRUE, TRUE);
    }
    catch (Exception $e) {
    }
  }

  return $map;
}

/**
 * Get list of file extensions which allowed for autoloading.
 *
 * @return string[]
 *   List of extensions.
 */
function autoload_extensions() {
  $extensions = array_filter(explode(',', spl_autoload_extensions()), 'trim');
  // Make sure the basic extensions are registered!
  $extensions[] = '.php';
  $extensions[] = '.inc';

  return array_unique($extensions);
}
